package LITS.Home_Four;

public class Team {

	private int idseriea;
	private String teams;
	private int points;
	
	public Team(int idseriea, String teams, int points) {
		super();
		this.idseriea = idseriea;
		this.teams = teams;
		this.points = points;
	}

	public int getId() {
		return idseriea;
	}

	public String getTeams() {
		return teams;
	}

	public int getPoints() {
		return points;
	}

	@Override
	public String toString() {
		return (idseriea + ". " + teams + "  " + points);
	}
	
}
