package LITS.Home_Four;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class JDBCConnector {

	static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost/jdbctask";
	
	static final String USER = "root";
	static final String PASS = "Password1";
		
	
	public static void main(String[] args) {
		Connection conn = null;
		Statement stmt = null;
		
		try {
			Class.forName(JDBC_DRIVER);
			System.out.println("Connection to DB...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			
			System.out.println("Creating table...");
			stmt = conn.createStatement();
			String sql;
			sql = "SELECT * FROM seriea";
			ResultSet rs = stmt.executeQuery(sql);
			
			List<Team> teamList= new ArrayList<Team>();
			
			while (rs.next()) {
				teamList.add(new Team(rs.getInt("idseriea"), rs.getString("teams"), rs.getInt("points")));
			}
		
			for (Team team : teamList) {
				System.out.println(team);
			}
			
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			}
			
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		System.out.println("Table is created!");
	}

}
